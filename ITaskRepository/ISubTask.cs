﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {
        public void CreateSubTask(SubTask subTask);

        public List<SubTask> GetAllSubTasks();

        public SubTask GetSubTaskById(int id);

    }
}
