﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {
        TaskDbContext _db;

        public TaskRepo(TaskDbContext db)
        {
            _db = db;

        }

        public void CreateTask(Tasks task)
        {
            _db.Tasks.Add(task);
            _db.SaveChanges();
        }

        public void DeleteTask(int id)
        {
            Tasks task = _db.Tasks.FirstOrDefault(x => x.Id == id);
            if (task != null)
            {
                _db.Tasks.Remove(task);
                _db.SaveChanges();
            }

        }

        public void EditTask(int id, Tasks task)
        {
            Tasks taskObj = _db.Tasks.FirstOrDefault(x => x.Id == id);
            if (taskObj != null)
            {
                foreach (Tasks temp in _db.Tasks)
                {
                    temp.Name = task.Name;
                    temp.Description = task.Description;
                    temp.CreatedBy = task.CreatedBy;
                    temp.CreatedOn = task.CreatedOn;

                }

            }
            _db.Tasks.Update(task);

        }

        public List<Tasks> GetAllTasks()
        {
            return _db.Tasks.ToList();
        }

        public Tasks GetTaskById(int id)
        {
            return _db.Tasks.FirstOrDefault(x => x.Id == id);
        }
    }
    }

